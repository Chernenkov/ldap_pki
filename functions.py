def ldapTimeToUnixTime(ldapTime):
    secsAfterADEpoch = ldapTime / 10000000
    ADToUnixConverter = ((1970 - 1601) * 365 - 3 + round((1970 - 1601) / 4)) * 86400
    return int(secsAfterADEpoch - ADToUnixConverter)