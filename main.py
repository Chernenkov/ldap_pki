import time
from os import system
import subprocess
# from shutil import *
import shutil

import functions

out_path = "/home/alex/PycharmProjects/ldap/out"
# write request to file
system(
    'echo $(ldapsearch -h # -p 32389 -x -D # -w # -b "DC=#,DC=#" "pwdLastSet") > /home/alex/PycharmProjects/ldap/out')
# /home/alex/PycharmProjects/ldap

# fields = list()
# read file's lines and split by entities
with open(out_path, "r") as base:
    fields = base.read()
    fields = fields.split("#")
    cleared = list()
    for string in fields:
        if string.find("pwdLastSet") != -1 and string.find("CN") != -1:
            cleared.append(string)
    cleared.pop(0)  # removes the string "requesting: pwdLastSet"

    # for string in cleared:
    #     print(string)

    ldap_time = 0
    bad_indexes = list()
    time_unchanged_in_years = list()
    cnt = 0
    for string in cleared:
        substring = string[-20:]
        if substring.find("pwdLastSet: 0") == -1:
            ldap_time = int(substring)  # parse the end of string to get LDAP Timestamp
        else:
            ldap_time = 0  # if it had text pwdLastSet: 0
        # count years of unchanged password
        time_unchanged_in_years \
            .append((int(time.time()) - functions.ldapTimeToUnixTime(ldap_time)) / (3600 * 24 * 7 * 4 * 12))
        # print("Time passed till now: ", time_unchanged_in_years[cnt])
        # print("Unix timestamp: ", functions.ldapTimeToUnixTime(integer_time))
        # print("LDAP timestamp: ", integer_time, '\n')
        if time_unchanged_in_years[cnt] > 5:
            bad_indexes.append(cnt)
        cnt += 1

        # make list of people, who have a pass, older than 5 years
    bad_passworded_guys = list()
    for idx in bad_indexes:
        bad_passworded_guys.append(cleared[idx])

    bad_passworded_guys = set(bad_passworded_guys)

    name = ""
    for string in bad_passworded_guys:

        name = string
        name = name.split("CN=")[1].split(",", 1)[0].replace(" ", "_")
        name = name.upper()
        # string = string.split("dn: ", 1)[1]
        print(name)
        string = '/C=RU/ST=IM/O=All/CN' + string.split("CN")[1].split(",", 1)[0].replace(" ", "_")
        # print(string)
        ###
        # Client-side
        ###
        # build a shell command for generating client keys
        command_client_keys_generate = "cd /home/alex/PycharmProjects/ldap/client_side && openssl genrsa -out " \
                                       + name \
                                       + ".key 1024"
        # print(command_client_keys_generate)
        # system(command_client_keys_generate)  ### !!!!!!!!!!!!

        # build a shell command for generating client CSRs
        command_client_csr_generate = "cd /home/alex/PycharmProjects/ldap/client_side && openssl req -new -key " \
                                      + name \
                                      + ".key -subj " \
                                      + string \
                                      + " -out " + name + ".csr"
        # print(command_client_csr_generate)
        # system(command_client_csr_generate)  ### !!!!!!!!!!!!
        ###
        # Server-side
        ###
        # build a command to sign clients' CSRs with SubCA's cert
        command_sign_csrs = "cd /home/alex/PycharmProjects/ldap/client_side &&" \
                            + " openssl x509 -req -in " \
                            + name \
                            + ".csr -CA /home/alex/PycharmProjects/ldap/server_side/ca/subca.crt " \
                            + "-CAkey /home/alex/PycharmProjects/ldap/server_side/ca/subca.key -CAcreateserial " \
                            + "-out " \
                            + name \
                            + ".crt -days 200"
        # print(command_sign_csrs)
        # system(command_sign_csrs)  ### !!!!!!!!!!!!
        # system(command_client_keys_generate)
        # system(command_client_csr_generate)
        system(command_sign_csrs)
    print(len(bad_indexes))
    print(len(bad_passworded_guys))
    print(len(cleared))
# /home/alex/PycharmProjects/ldap

# Client side actions: done
# openssl genrsa -out client1.suai.key 4096
# openssl req -new -key client1.suai.key -subj "/C=RU/ST=IM/O=SUAI/CN=client1.suai" -out client1.suai.csr

# Sub-CA side actions:
# openssl x509 -req -in client1.suai.csr -CA ../intermediate/intermediate.crt -CAkey ../intermediate/intermediate.key -CAcreateserial -out client1.suai.crt -days 200
